package crud;

import model.ClientEntity;
import model.ComandaEntity;
import model.DeptEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;

public class DeptCRUD {
    public static List<DeptEntity> getDepartaments(Session session) {
        List<DeptEntity> depts = session.createQuery("SELECT a FROM DeptEntity a", DeptEntity.class).getResultList();

        for (Object o : depts) {
            System.out.println("  " + o);
        }

        session.close();
        System.exit(0);
        return depts;
    }

    public static List getAll(Session session){
        List depts;
        Criteria crit = session.createCriteria(DeptEntity.class);
        depts = crit.list();
        return depts;
    }
}
