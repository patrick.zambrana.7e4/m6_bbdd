package crud;

import model.ComandaEntity;
import model.DeptEntity;
import model.EmpEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;

public class EmpCRUD {
    public static List<EmpEntity> getDepartaments(Session session) {
        List<EmpEntity> emps = session.createQuery("SELECT a FROM EmpEntity a", EmpEntity.class).getResultList();

        for (Object o : emps) {
            System.out.println("  " + o);
        }

        session.close();
        System.exit(0);
        return emps;
    }

    public static List getAll(Session session){
        List emps;
        Criteria crit = session.createCriteria(EmpEntity.class);
        emps = crit.list();
        return emps;
    }
}
