package crud;

import model.ClientEntity;
import model.ComandaEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;

public class ClientCRUD {
    public static List<ClientEntity> getClients(Session session) {
        List<ClientEntity> clients = session.createQuery("SELECT a FROM ClientEntity a", ClientEntity.class).getResultList();

        for (Object o : clients) {
            System.out.println("  " + o);
        }

        session.close();
        System.exit(0);
        return clients;
    }

    public static List getAll(Session session){
        List clients;
        Criteria crit = session.createCriteria(ClientEntity.class);
        clients = crit.list();
        return clients;
    }
}
