package crud;

import model.ComandaEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;


public class ComandaCRUD {
    public static List<ComandaEntity> getComandes(Session session) {
        List<ComandaEntity> comandes = session.createQuery("SELECT a FROM ComandaEntity a", ComandaEntity.class).getResultList();

        for (Object o : comandes) {
            System.out.println("  " + o);
        }

        session.close();
        System.exit(0);
        return comandes;
    }

    public static List getAll(Session session){
        List comandas;
        Criteria crit = session.createCriteria(ComandaEntity.class);
        comandas = crit.list();
        return comandas;
    }
}
