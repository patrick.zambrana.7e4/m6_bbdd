package EA1;

import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainEA1 {

    private static Connection conexion;

    static {
        try {
            conexion = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        int option = -1;
        while (true) {
            System.out.println("\nMenú:\n" +
                    "1) Ex1: Mostra les dades de la taula 'Asignaturas'\n" +
                    "2) Ex2: Mostra les dades de la taula 'Alumnos'\n" +
                    "3) Ex3: Mostra les dades de la taula NOTAS\n" +
                    "4) Ex4: Mostra les notes de l’alumne amb DNI «4448242» de la taula NOTAS\n" +
                    "5) Ex5: Inserta 3 alumnes nous.\n" +
                    "6) Ex6: Inserta les notes per aquests 3 nous alumnes de les assignatures FOL i RET. Tots han tret un 8 en les dues assignatures.\n" +
                    "7) Ex7: Modificar les notes de l’alumne \"Cerrato Vela, Luis\" de FOL i RET, ha tret un 9.\n" +
                    "8) Ex8: Modificar el teléfon utilitzant paràmetres de l’alumne amb DNI = 12344345, el nou teléfon és 934885237.\n" +
                    "9) Ex9: Elimina l'alumne que viu a mostoles.\n" +
                    "0) Acaba el programa.");
            option = new Scanner(System.in).nextInt();
            if (option == 0)
                break;
            switch (option) {
                case 1:
                    ex1();
                    break;
                case 2:
                    ex2();
                    break;
                case 3:
                    ex3();
                    break;
                case 4:
                    ex4();
                    break;
                case 5:
                    ex5();
                    break;
                case 6:
                    ex6();
                    break;
                case 7:
                    ex7();
                    break;
                case 8:
                    ex8();
                    break;
                case 9:
                    ex9();
                    break;
            }
        }
        //conexion.close();
    }

    private static void ex1() {
        System.out.println("\n- - - - - EXERCICI 1 - - - - -\n");
        consultaSQL("SELECT * FROM asignaturas");
    }

    private static void ex2() {
        System.out.println("\n- - - - - EXERCICI 2 - - - - -\n");
        consultaSQL("SELECT * FROM alumnos");
    }

    /**
     * Mostrar totes les dades de la taula NOTAS.
     */
    private static void ex3() {
        System.out.println("\n- - - - - EXERCICI 3 - - - - -\n");
        consultaSQL("SELECT * FROM notas");
    }

    /**
     * Mostrar les notes de l’alumne amb DNI «4448242» de la taula NOTAS.
     */
    private static void ex4() {
        System.out.println("\n- - - - - EXERCICI 4 - - - - -\n");
        consulta_preparedStatement("SELECT * FROM notas WHERE dni like ?", 1, "4448242");
    }

    /**
     * Insertar 3 alumnes nous.
     */
    private static void ex5() {
        System.out.println("\n- - - - - EXERCICI 5 - - - - -\n");
        try{
            Statement stmt = conexion.createStatement();
            List<String> inserts = Arrays.asList("INSERT INTO ALUMNOS VALUES ('12325311','Colomer Vila, Roc', 'C/ Estopenya, 24','Barcelona','935578899')",
                    "INSERT INTO ALUMNOS VALUES ('42386721', 'Font Gravera, Ariadna', 'C/ Blausina, 2', 'Girona', '924013745')",
                    "INSERT INTO ALUMNOS VALUES ('83072893', 'Garcia Lopez, Raul', 'C/ Maurici Clair, 91', 'Valencia', '9831678234')");
            for (String insert : inserts)
                stmt.executeUpdate(insert);
            stmt.close();
            conexion.close();
            System.out.println("Alumnes afegits correctament.");
        }catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }catch(Exception ex){
            System.out.println("Exception: " + ex.getMessage());
        }
    }

    /**
     *Insertar les notes per aquests 3 nous alumnes de les assignatures FOL i RET. Tots han tret un 8 en les dues
     * assignatures. Utilitza la Classe de JDBC "PreparedStatement" seguint l'exemple de la classe
     * "InsertaDepPreparedStatement.java".
     */
    private static void ex6() {
        System.out.println("\n- - - - - EXERCICI 6 - - - - -\n");
        try {
            // construir ordenes INSERT
            List<String> inserts = Arrays.asList("INSERT INTO notas VALUES('12325311', '4', '8')",
                    "INSERT INTO notas VALUES('12325311', '5', '8')",
                    "INSERT INTO notas VALUES('42386721', '4', '8')",
                    "INSERT INTO notas VALUES('42386721', '5', '8')",
                    "INSERT INTO notas VALUES('83072893', '4', '8')",
                    "INSERT INTO notas VALUES('83072893', '5', '8')");

            PreparedStatement sentencia = null;
            /*
            sentencia.setInt(1, Integer.parseInt(dep));
            sentencia.setString(2, dnombre);
            sentencia.setString(3, loc);
            */

            int filas = 0;
            try {
                for (String insert : inserts) {
                    sentencia = conexion.prepareStatement(insert);
                    filas = sentencia.executeUpdate();
                    filas++;
                }
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    "+ e.getMessage());
                System.out.println("SQL estado: "+ e.getSQLState());
                System.out.println("C�d error:  "+ e.getErrorCode());
            }

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Modificar les notes de l’alumne "Cerrato Vela, Luis" de FOL i RET, ha tret un 9. Segueix l'exemple de la
     * classe "Update.java"
     */
    private static void ex7() {
        System.out.println("\n- - - - - EXERCICI 7 - - - - -\n");
        try {
            //construir orden INSERT
            String sql = "UPDATE NOTAS SET NOTA = 9 WHERE DNI = '4448242'";

            System.out.println(sql);

            System.out.println(sql);
            Statement sentencia = conexion.createStatement();
            int filas=0;
            try {
                filas = sentencia.executeUpdate(sql.toString());
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                //e.printStackTrace();
                System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
                System.out.printf("Mensaje   : %s %n", e.getMessage());
                System.out.printf("SQL estado: %s %n", e.getSQLState());
                System.out.printf("C�d error : %s %n", e.getErrorCode());
            }

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Modificar el teléfon utilitzant paràmetres de l’alumne amb DNI = 12344345, el nou teléfon és 934885237.
     * Segueix l'exemple de la classe "ModificarSalario.java".
     */
    private static void ex8() {
        System.out.println("\n- - - - - EXERCICI 8 - - - - -\n");
        String dni = "12344345", tlf ="934885237";

        try {
            String sql = "UPDATE alumnos SET telef = '934885237' WHERE dni = '12344345'";

            System.out.println(sql);

            Statement sentencia = conexion.createStatement();
            int filas = sentencia.executeUpdate(sql);
            System.out.printf("Empleados modificados: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n

        } catch (SQLException e) {
            if (e.getErrorCode() == 1062)
                System.out.println("CLAVE PRIMARIA DUPLICADA");
            else
            if (e.getErrorCode() == 1452)
                System.out.println("CLAVE AJENA "+ dni + " NO EXISTE");

            else {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("C�d error:  " + e.getErrorCode());
            }
        }
    }


    /**
     * Elimina l'alumne que viu a mostoles.
     */
    private static void ex9() {
        System.out.println("\n- - - - - EXERCICI 9 - - - - -\n");
        consultaSQL("DELETE FROM alumnos WHERE pobla = 'Mostoles'");
    }



    // - - - - - -- - - -- - - - - - - -

    /**
     * Consulta básica d'SQL
     * @param sql
     */
    public static void consultaSQL(String sql) {


        try {
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while (rs.next())
                    System.out.printf("COD: %d, %s, %n",
                            rs.getInt(1), rs.getString(2));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

            sentencia.close();
            conexion.close();
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }

    /**
     * Consulta SQL amb 1 paràmetre.
     * @param query
     */
    public static void consulta_preparedStatement(String query, int paramIndex, String parameter) {
        //CREAR UN ESTATEMENT PARA UNA CONSULTA SELECT CON PARÁMETROS
        try {
            PreparedStatement pst = conexion.prepareStatement(query);
            pst.setString(paramIndex, parameter);

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                System.out.println("Dni - " + rs.getInt("dni") +
                        ", Cod " + rs.getInt("cod") +
                        ", Nota " + rs.getInt("nota"));
            }
            rs.close();
            pst.close();
        } catch (SQLException ex) {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
        }
    }
}
