package EA2;

import java.io.*;
import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainEA2 {

    private static List<String> tables = Arrays.asList("alumnos", "asignaturas", "notas");
    private static Connection connection;

    public static void main(String[] args) throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school2","school2", "school2");
        int option = -1;
        while (true) {
            System.out.println("\nMenú:\n" +
                    "1) Ex1: Executar el script \"school2.sql\"\n" +
                    "2) Ex2: Mostrar la informació de la base de dades.\n" +
                    "3) Ex3: Mostra la informació dels camps de totes les taules creades.\n" +
                    "4) Ex4: Mostra la informació dels camps de totes les taules creades amb un altre metode.\n" +
                    "5) Ex5: Mostra les claus primaries de totes les taules.\n" +
                    "6) Ex6: Mostra les claus foràneas de totes les taules.\n" +
                    "0) Acaba el programa.");
            option = new Scanner(System.in).nextInt();
            if (option == 0)
                break;
            switch (option) {
                case 1:
                    ex1();
                    break;
                case 2:
                    ex2();
                    break;
                case 3:
                    ex3();
                    break;
                case 4:
                    ex4();
                    break;
                case 5:
                    ex5();
                    break;
                case 6:
                    ex6();
                    break;
            }
        }
        connection.close();
    }

    /**
     * Executa el script "school2.sql"
     */
    public static void ex1() {
        ejecutarScriptSQL();
    }

    /**
     * Mostra la informació de la base de dades.
     */
    private static void ex2() {
        try
        {
            DatabaseMetaData dbmd = connection.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String nombre  = dbmd.getDatabaseProductName();
            String driver  = dbmd.getDriverName();
            String url     = dbmd.getURL();
            String usuario = dbmd.getUserName() ;

            System.out.println("INFORMACION SOBRE LA BASE DE DATOS:");
            System.out.println("===================================");
            System.out.printf("Nombre : %s %n", nombre );
            System.out.printf("Driver : %s %n", driver );
            System.out.printf("URL    : %s %n", url );
            System.out.printf("Usuario: %s %n", usuario );

            //Obtener informaci�n de las tablas y vistas que hay
            resul = dbmd.getTables(null, "ejemplo", null, null);

            while (resul.next()) {
                String catalogo = resul.getString(1);//columna 1
                String esquema = resul.getString(2); //columna 2
                String tabla = resul.getString(3);   //columna 3
                String tipo = resul.getString(4);	  //columna 4
                System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n",
                        tipo, catalogo, esquema, tabla);
            }
        } catch (SQLException e) {e.printStackTrace();}
    }

    /**
     * Mostra la informació dels camps de totes les taules creades.
     */
    private static void ex3() {
        try {
            Statement sentencia = connection.createStatement();
            List<String> querys = Arrays.asList("SELECT * FROM alumnos", "SELECT * FROM asignaturas", "SELECT * FROM notas");
            ResultSet rs = null;
            for (String query : querys) {
                System.out.println("===================================");
                rs = sentencia.executeQuery(query);
                ResultSetMetaData rsmd = rs.getMetaData();
                int nColumnas = rsmd.getColumnCount();
                String nula;
                System.out.printf("Numero de columnas recuperadas: %d%n", nColumnas);
                for (int i = 1; i <= nColumnas; i++) {
                    System.out.printf("Columna %d: %n ", i);
                    System.out.printf("  Nombre: %s %n   Tipo: %s %n ",
                            rsmd.getColumnName(i),  rsmd.getColumnTypeName(i));
                    if (rsmd.isNullable(i) == 0)
                        nula = "NO";
                    else
                        nula = "SI";

                    System.out.printf("  Puede ser nula?: %s %n ", nula);
                    System.out.printf("  Maximo ancho de la columna: %d %n", rsmd.getColumnDisplaySize(i));
                }// for
                System.out.println("===================================\n");
            }
            sentencia.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Mostra la informació dels camps de totes les taules creades seguint altre metode.
     */
    private static void ex4() {
        try {
            DatabaseMetaData dbmd = connection.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;
            for (String table : tables) {
                System.out.println("\nCOLUMNAS TABLA " + table);
                System.out.println("===================================");
                ResultSet columnas=null;
                columnas = dbmd.getColumns(null, null, table, null);
                while (columnas.next()) {
                    String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                    String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                    String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                    String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                    System.out.printf("Columna: %s, Tipo: %s, Tamaño: %s, ¿Puede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
                }
            }
        } catch (SQLException e) {e.printStackTrace();}
    }

    /**
     * Mostra les claus primaries de totes les taules.
     */
    private static void ex5() {
        try {
            DatabaseMetaData dbmd = connection.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;
            for (String table : tables) {
                System.out.println("\nCLAVE PRIMARIA TABLA " + table);
                System.out.println("===================================");
                ResultSet pk = dbmd.getPrimaryKeys(null, null, table);
                String pkDep="", separador="";
                while (pk.next()) {
                    pkDep = pkDep + separador +
                            pk.getString("COLUMN_NAME");//getString(4)
                    separador="+";
                }
                System.out.println("Clave Primaria: " + pkDep);
            }
        } catch (SQLException e) {e.printStackTrace();}
    }

    /**
     * Mostra les claus foràneas de totes les taules.
     */
    private static void ex6() {
        try {
            DatabaseMetaData dbmd = connection.getMetaData();// Creamos
            // objeto DatabaseMetaData
            ResultSet resul = null;
            for (String table : tables) {
                System.out.println("\nCLAVES ajenas que referencian a " + table);
                System.out.println("==============================================");
                ResultSet fk = dbmd.getExportedKeys(null, null, table);

                while (fk.next()) {
                    String fk_name = fk.getString("FKCOLUMN_NAME");
                    String pk_name = fk.getString("PKCOLUMN_NAME");
                    String pk_tablename = fk.getString("PKTABLE_NAME");
                    String fk_tablename = fk.getString("FKTABLE_NAME");
                    System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                    System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    // ---------------------------------------------------

    public static void ejecutarScriptSQL() {
        File scriptFile = new File("school2.sql");
        System.out.println("\n\nFichero de consulta : " + scriptFile.getName());
        System.out.println("Convirtiendo el fichero a cadena...");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(scriptFile));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR Driver:" + e.getMessage());
        }
        try {
            Statement sents = connection.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }
}
