package EA5;

import crud.ClientCRUD;
import crud.ComandaCRUD;
import crud.EmpCRUD;
import model.*;
import org.hibernate.Criteria;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.util.List;
import java.util.Scanner;

public class MainEA5 {

    private static Session session;

    public static void main(String[] args) {
        session = new ConnexioEmpresa().getSession();
        try {
            int option = -1;
            while (true) {
                System.out.println("\nMenú:\n" +
                        "1) Ex1: Mostra tota la informació de totes les comandes.\n" +
                        "2) Ex2: Mostra el nom del client (client_cod) i la data de comanda (com_data) de les comandes que el camp com_tipus sigui null\n" +
                        "3) Ex3: Mostra el nom i el telèfon dels clients de la ciutat de BURLINGAME\n" +
                        "4) Ex4: Mostra tota la informació de l’empleat amb cognom CEREZO\n" +
                        "5) Ex5: Mostra el codi (com_num) de les comandes que la data de tramesa sigui entre el 1 de gener del 1986 i el 30 de setembre del 1986. Utilitza el CriteriaBuilder.between.\n" +
                        "6) Ex6: Mostra el cognom i l’ofici dels empleats que cobrin més de 300.000.\n" +
                        "7) Ex7: Mostra quants empleats té cada departament. Utilitza el CriteriaQuery.groupBy\n" +
                        "0) Acaba el programa.");
                option = new Scanner(System.in).nextInt();
                if (option == 0)
                    break;
                switch (option) {
                    case 1:
                        ex1();
                        break;
                    case 2:
                        ex2();
                        break;
                    case 3:
                        ex3();
                        break;
                    case 4:
                        ex4();
                        break;
                    case 5:
                        ex5();
                        break;
                    case 6:
                        ex6();
                        break;
                    case 7:
                        ex7();
                        break;
                }
            }

        } finally {
            session.close();
        }
    }

    private static void ex1() {
        List<ComandaEntity> comandas = ComandaCRUD.getAll(session);
        for (ComandaEntity comanda : comandas) {
            System.out.println(comanda);
        }
    }

    private static void ex2() {
        List<ComandaEntity> comandas = ComandaCRUD.getAll(session);
        List<ClientEntity> clients = ClientCRUD.getAll(session);
        int i = 0;
        for (ComandaEntity comanda : comandas) {
            if (comanda.getComTipus() == null) {
                //clients.stream().filter(c -> c.getClientCod() == comanda.getClientCod())
                System.out.println(comanda);
            }
        }
    }

    private static void ex3() {
        List<ClientEntity> clients = ClientCRUD.getAll(session);
        for (ClientEntity client : clients) {
            if (client.getCiutat().equals("BURLINGAME"))
                System.out.println(String.format("Nom: %s, Tlf: %s", client.getNom(), client.getTelefon()));
        }
    }

    private static void ex4() {
        List<EmpEntity> emps = EmpCRUD.getAll(session);
        for (EmpEntity emp : emps) {
            if (emp.getCognom().equals("CEREZO")){
                System.out.println(emp);
                break;
            }
        }
    }

    private static void ex5() {
    }

    private static void ex6() {
        List<EmpEntity> emps = EmpCRUD.getAll(session);
        for (EmpEntity emp : emps) {
            if (emp.getSalari().intValue() > 300000){
                System.out.println(String.format("Cognom: %s, Ofici: %s", emp.getCognom(), emp.getOfici()));
                break;
            }
        }
    }

    private static void ex7() {
        List<DeptEntity> depts = EmpCRUD.getAll(session);
        for (DeptEntity dept : depts) {
            System.out.println(dept.getDeptNo());
        }
    }
}
