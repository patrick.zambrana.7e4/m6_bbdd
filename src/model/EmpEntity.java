package model;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "emp", schema = "public", catalog = "jdjtbtfw")
public class EmpEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "emp_no", nullable = false, precision = 0)
    private int empNo;
    @Basic
    @Column(name = "cognom", nullable = false, length = 10)
    private String cognom;
    @Basic
    @Column(name = "ofici", nullable = true, length = 10)
    private String ofici;
    @Basic
    @Column(name = "data_alta", nullable = true)
    private Date dataAlta;
    @Basic
    @Column(name = "salari", nullable = true, precision = 0)
    private BigInteger salari;
    @Basic
    @Column(name = "comissio", nullable = true, precision = 0)
    private BigInteger comissio;

    public int getEmpNo() {
        return empNo;
    }

    public void setEmpNo(int empNo) {
        this.empNo = empNo;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public Date getDataAlta() {
        return dataAlta;
    }

    public void setDataAlta(Date dataAlta) {
        this.dataAlta = dataAlta;
    }

    public BigInteger getSalari() {
        return salari;
    }

    public void setSalari(BigInteger salari) {
        this.salari = salari;
    }

    public BigInteger getComissio() {
        return comissio;
    }

    public void setComissio(BigInteger comissio) {
        this.comissio = comissio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpEntity empEntity = (EmpEntity) o;
        return empNo == empEntity.empNo && Objects.equals(cognom, empEntity.cognom) && Objects.equals(ofici, empEntity.ofici) && Objects.equals(dataAlta, empEntity.dataAlta) && Objects.equals(salari, empEntity.salari) && Objects.equals(comissio, empEntity.comissio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empNo, cognom, ofici, dataAlta, salari, comissio);
    }

    @Override
    public String toString() {
        return "EmpEntity{" +
                "empNo=" + empNo +
                ", cognom='" + cognom + '\'' +
                ", ofici='" + ofici + '\'' +
                ", dataAlta=" + dataAlta +
                ", salari=" + salari +
                ", comissio=" + comissio +
                '}';
    }
}
