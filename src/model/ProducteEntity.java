package model;

import javax.persistence.*;

@Entity
@Table(name = "producte", schema = "public", catalog = "jdjtbtfw")
public class ProducteEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "prod_num", nullable = false, precision = 0)
    private int prodNum;
    @Basic
    @Column(name = "descripcio", nullable = false, length = 30)
    private String descripcio;

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }
}
