package model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "detall", schema = "public", catalog = "jdjtbtfw")
@IdClass(DetallEntityPK.class)
public class DetallEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "com_num", nullable = false, precision = 0)
    private int comNum;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "detall_num", nullable = false, precision = 0)
    private int detallNum;
    @Basic
    @Column(name = "preu_venda", nullable = true, precision = 2)
    private BigDecimal preuVenda;
    @Basic
    @Column(name = "quantitat", nullable = true, precision = 0)
    private Integer quantitat;
    @Basic
    @Column(name = "import", nullable = true, precision = 2)
    private BigDecimal import_;

    public int getComNum() {
        return comNum;
    }

    public void setComNum(int comNum) {
        this.comNum = comNum;
    }

    public int getDetallNum() {
        return detallNum;
    }

    public void setDetallNum(int detallNum) {
        this.detallNum = detallNum;
    }

    public BigDecimal getPreuVenda() {
        return preuVenda;
    }

    public void setPreuVenda(BigDecimal preuVenda) {
        this.preuVenda = preuVenda;
    }

    public Integer getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(Integer quantitat) {
        this.quantitat = quantitat;
    }

    public BigDecimal getImport_() {
        return import_;
    }

    public void setImport_(BigDecimal import_) {
        this.import_ = import_;
    }
}
