package model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "client", schema = "public", catalog = "jdjtbtfw")
public class ClientEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "client_cod", nullable = false, precision = 0)
    private int clientCod;
    @Basic
    @Column(name = "nom", nullable = false, length = 45)
    private String nom;
    @Basic
    @Column(name = "adreca", nullable = false, length = 40)
    private String adreca;
    @Basic
    @Column(name = "ciutat", nullable = false, length = 30)
    private String ciutat;
    @Basic
    @Column(name = "estat", nullable = true, length = 2)
    private String estat;
    @Basic
    @Column(name = "codi_postal", nullable = false, length = 9)
    private String codiPostal;
    @Basic
    @Column(name = "area", nullable = true, precision = 0)
    private Integer area;
    @Basic
    @Column(name = "telefon", nullable = true, length = 9)
    private String telefon;
    @Basic
    @Column(name = "limit_credit", nullable = true, precision = 2)
    private BigDecimal limitCredit;
    @Basic
    @Column(name = "observacions", nullable = true, length = -1)
    private String observacions;

    public int getClientCod() {
        return clientCod;
    }

    public void setClientCod(int clientCod) {
        this.clientCod = clientCod;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getEstat() {
        return estat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public String getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public BigDecimal getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(BigDecimal limitCredit) {
        this.limitCredit = limitCredit;
    }

    public String getObservacions() {
        return observacions;
    }

    public void setObservacions(String observacions) {
        this.observacions = observacions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity that = (ClientEntity) o;
        return clientCod == that.clientCod && Objects.equals(nom, that.nom) && Objects.equals(adreca, that.adreca) && Objects.equals(ciutat, that.ciutat) && Objects.equals(estat, that.estat) && Objects.equals(codiPostal, that.codiPostal) && Objects.equals(area, that.area) && Objects.equals(telefon, that.telefon) && Objects.equals(limitCredit, that.limitCredit) && Objects.equals(observacions, that.observacions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientCod, nom, adreca, ciutat, estat, codiPostal, area, telefon, limitCredit, observacions);
    }
}
