package model;

import java.io.Serializable;
import java.util.Objects;

public class DetallEntityPK implements Serializable {
    private int comNum;
    private int detallNum;

    public int getComNum() {
        return comNum;
    }

    public void setComNum(int comNum) {
        this.comNum = comNum;
    }

    public int getDetallNum() {
        return detallNum;
    }

    public void setDetallNum(int detallNum) {
        this.detallNum = detallNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetallEntityPK that = (DetallEntityPK) o;
        return comNum == that.comNum && detallNum == that.detallNum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(comNum, detallNum);
    }
}
