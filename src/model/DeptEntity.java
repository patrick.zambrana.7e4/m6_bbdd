package model;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;

@Entity
@Table(name = "dept", schema = "public", catalog = "jdjtbtfw")
public class DeptEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "dept_no", nullable = false, precision = 0)
    private BigInteger deptNo;
    @Basic
    @Column(name = "dnom", nullable = false, length = 14)
    private String dnom;
    @Basic
    @Column(name = "loc", nullable = true, length = 14)
    private String loc;

    public BigInteger getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(BigInteger deptNo) {
        this.deptNo = deptNo;
    }

    public String getDnom() {
        return dnom;
    }

    public void setDnom(String dnom) {
        this.dnom = dnom;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeptEntity that = (DeptEntity) o;
        return Objects.equals(deptNo, that.deptNo) && Objects.equals(dnom, that.dnom) && Objects.equals(loc, that.loc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptNo, dnom, loc);
    }

    @Override
    public String toString() {
        return "DeptEntity{" +
                "deptNo=" + deptNo +
                ", dnom='" + dnom + '\'' +
                ", loc='" + loc + '\'' +
                '}';
    }
}
