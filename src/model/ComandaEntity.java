package model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "comanda", schema = "public", catalog = "jdjtbtfw")
public class ComandaEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "com_num", nullable = false, precision = 0)
    private int comNum;
    @Basic
    @Column(name = "com_data", nullable = true)
    private Date comData;
    @Basic
    @Column(name = "com_tipus", nullable = true, length = -1)
    private String comTipus;
    @Basic
    @Column(name = "data_tramesa", nullable = true)
    private Date dataTramesa;
    @Basic
    @Column(name = "total", nullable = true, precision = 2)
    private BigDecimal total;

    public int getComNum() {
        return comNum;
    }

    public void setComNum(int comNum) {
        this.comNum = comNum;
    }

    public Date getComData() {
        return comData;
    }

    public void setComData(Date comData) {
        this.comData = comData;
    }

    public String getComTipus() {
        return comTipus;
    }

    public void setComTipus(String comTipus) {
        this.comTipus = comTipus;
    }

    public Date getDataTramesa() {
        return dataTramesa;
    }

    public void setDataTramesa(Date dataTramesa) {
        this.dataTramesa = dataTramesa;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ComandaEntity{" +
                "comNum=" + comNum +
                ", comData=" + comData +
                ", comTipus='" + comTipus + '\'' +
                ", dataTramesa=" + dataTramesa +
                ", total=" + total +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComandaEntity that = (ComandaEntity) o;
        return comNum == that.comNum && Objects.equals(comData, that.comData) && Objects.equals(comTipus, that.comTipus) && Objects.equals(dataTramesa, that.dataTramesa) && Objects.equals(total, that.total);
    }

    @Override
    public int hashCode() {
        return Objects.hash(comNum, comData, comTipus, dataTramesa, total);
    }
}
